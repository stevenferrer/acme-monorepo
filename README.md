# acme-monorepo

This is a very simple example of TypeScript monorepo.

## Limitations

- [Yarn workspace fails to add local package as dependency](https://github.com/yarnpkg/yarn/issues/4878)
